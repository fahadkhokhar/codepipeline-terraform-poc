# dotCMS Infrastructure Creation - AWS Terraform

Instructions on how to run the terraform scripts to automate creation of dotCMS environments in AWS

## Before you run

This project leverages Terraform and Amazon Web Services to spin up a cloud dotCMS environment.

[Terraform Documentation](https://www.terraform.io/)

[AWS Documentation](https://docs.aws.amazon.com/index.html?nc2=h_ql_doc)

### Configuring your AWS CLI

[AWS CLI Documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html)

* NOTE - you can have multiple named AWS profiles - this is the easiest way to maintain access for multiple AWS accounts. The AWS CLI documentation covers this in detail.

### Installing Terraform

[Terraform Installation Documentation](https://learn.hashicorp.com/terraform/getting-started/install.html)

* NOTE - this project expects you to be running [Terraform 0.12](https://www.hashicorp.com/blog/announcing-terraform-0-12 "Terraform 0.12") - if you have previously installed terraform on your machine, ensure that you have the correct version by running ```terraform version```

## dotCMS Infrastructure Creation - Introduction

This repository contains code that will allow a developer to automatically create the dotCMS stack. The stack will contain:

* A public facing load balancer
* A PostgreSQL RDS instance, with a read only replica
* An autoscaling group with a launch template that installs the latest (or specified) version of dotCMS
* An elastic file system that will contain the asset files

(The RDS instance and EFS are shared by all the EC2 instances which allows [clustering](https://dotcms.com/docs/latest/cluster-setup))

This script assumes that you already have an AWS access key and a secret configured with administrative privileges.

## Tasks

### Running

The environments directory holds the configuration for individual environments. In order to create or update an environment, there are just a few steps.

* Change to the directory of the environment.
* If you haven't run this environment before on the local machine, run '`./init.sh`
* Run `terraform apply`.

### Tear Down

An environment can be destroyed by running `terraform destroy`. You will be shown what resources will be destroyed and be given the option to continue or abort the operation. Destroy should be executed carefully, to ensure that you don't destroy the wrong environment.

### Creating a brand new environment

The `environments` directory is meant to serve as a template.

Example Use Case:

Create a new 'multinode' aws environment named fresh-multinode.

1. Copy the `multinode` directory to a new directory under environments - name the new directory `fresh-multinode` for consistency

2. Modify the main.tf file as needed (see `main.tf` section below) - if you are running in the same aws region as the multinode environment you are copying, the only thing that needs changed is the `env` var. Change this to your newly named `fresh-multinode` - i.e.

    ```hcl-terraform
    locals {
      vpc-id                      = "vpc-123456789abc"
      public-subnet-ids           = ["subnet-123456789abc", "subnet-123456789def", "subnet-123456789ghi"]
      private-subnet-ids          = ["subnet-123456789jkl", "subnet-123456789mno", "subnet-123456789pqr"]
      public-key                  = "../../dotcms-ec2-key.pub"
      env                         = "fresh-multinode"
      aws-region                  = "us-east-1"
      dotcms-version              = "5.1.5"
      license-software-s3-bucket  = "com-amwater-dotcms-software-np"
      ec2-launch-arn              = "arn:aws:iam::462393762422:instance-profile/ec2-dotcms"
    }
    ```

    * _IMPORTANT_ If you are copying a directory that has previously been initialized, be sure to delete the .terraform directory as this will cause issues

3. Modify the `init.sh` script - the responsibility of this script is 2-fold:  
    * Runs the [`terraform init`](https://www.terraform.io/docs/commands/init.html) command which initializes your backend on your local disk (in the `.terraform` directory) and all of the modules referenced in your `main.tf` file  
    * Passes options to the `terraform init` command to put your `.tfstate` file into an S3 bucket - this is what allows you to share state between multiple machines referencing the same terraformed environment  
    For our `fresh-multinode` example, your `init.sh` script should contain the following:  
    ```terraform init -backend-config="bucket=com-amwater-terraform-state" -backend-config="region=us-east-1" -backend-config="key=dotcms-amwater-fresh-multinode.tfstate"```  
  Requirements:  
    * S3 bucket must exist in the specified region (i.e. `-backend-config="region...` - not to be confused with the `aws-region` variable in the locals block of `main.tf`)  
    * Key for the .tfstate file must be unique - name it according to the environment name - can also include the region - the name should not leave any confusion as to which environment it references  

4. Navigate to your new  `fresh-multinode` environment and run `./init.sh` - you should see a new `.terraform` directory in your environment folder

5. (Optional) Run `terraform plan`  
    * This will give you an output of the resources that terraform will create, any errors, etc.  

6. Run `terraform apply`  
    * This will actually run the terraform script to create your resources. Note that the first part of this command runs the `plan` command mentioned above and prompts you to proceed.  

Refer to the Initialization section for updating `init.sh`. This controls where the environment backend is stored and will contain your state file.

For `main.tf`:

* `aws-region` the region this environment will use - you should ensure that you have adequate permissions for your AWS account in this region  
* `vpc-id` should be the ID of an existing VPC in your AWS environment and in the specified region  
* `public/private-subnet-ids` should be a list of subnets IDs available in that VPC - the EC2 instances and Load Balancer will sit in the public subnet and all other resources will sit in the private subnet - if using default subnets, you can use the same array of subnets for each of these variables  
* `env` is the name of your environment and will be appended to all created resources. This should be unique to avoid conflicts
* `ec2-launch-arn` - this is the instance profile that gets attached to the EC2 instances - the user data script leverages an `aws s3 cp` command on the `license-software-s3-bucket` - this instance profile must have read access to the bucket specified by this variable  

Example `local` variable block:

```hcl-terraform
//Virginia - Running with AmWater - Amwater platform vpc
locals {
  vpc-id                      = "vpc-00fe7a267b76f6697"
  public-subnet-ids           = ["subnet-0adf29c90214ea15b", "subnet-01fc19fb69721da9e", "subnet-03c1ff4ae097e6a6b"]
  private-subnet-ids          = ["subnet-0f5f6c7f978d5a4dc", "subnet-0b3f0ba2beaddbd2a", "subnet-0405e508723defedb"]
  public-key                  = "../../dotcms-ec2-key.pub"
  env                         = "nonprod"
  aws-region                  = "us-east-1"
  dotcms-version              = "5.1.5"
  license-software-s3-bucket  = "com-amwater-dotcms-software-np"
  ec2-launch-arn              = "arn:aws:iam::462393762422:instance-profile/ec2-dotcms"
}
```

This file also contains variables for each module it will be creating

```hcl-terraform
module  "regulated-node"  {
  source                      = "../../modules/dotcms-node"
  aws-region                  = local.aws-region
  client-name                 = "regulated"
  env                         = local.env
  vpc-id                      = local.vpc-id
  load-balancer-inbound-cidr  = ["161.69.122.12/32", "35.175.3.10/32"]
  public-subnet-ids           = local.public-subnet-ids
  private-subnet-ids          = local.private-subnet-ids
  rds-user                    = "dev_admin"
  rds-password                = "${module.regulated-password.password}"
  rds-instance-class          = "db.t3.large"
  rds-allocated-storage       = "20"
  public-key                  = local.public-key
  ec2-instance-type           = "t3.medium"
  ec2-launch-arn              = local.ec2-launch-arn
  license-file-name           = "dotCMSLicensePack1561555701486.zip"
  license-software-s3-bucket  = local.license-software-s3-bucket
  scaling-max                 = "3"
  scaling-min                 = "2"
  autoscaling-linked-role-arn = local.autoscaling-linked-role-arn
  ssh-security-group          = local.ssh-security-group-id
  dotcms-version              = local.dotcms-version
  rds-snapshot-id             = "arn:aws:rds:us-east-1:719620547956:snapshot:ir-dotcms-091619"
  rds-subnet-group-id         = "${module.db-subnet-group.subnet-group-id}"
}
```

* `rds-snapshot-id` is an optional variable. It needs to be the ID of an existing snapshot. If provided, it will create the RDS instance using that snapshot. If nothing is provided, it will create a fresh RDS instance.
* `dotcms-version` The version of dotCMS to pull from the S3 bucket, `com-amwater-dotcms-software`. The expected name is `dotcms_DOTCMS-VERSION.tar.gz`
  * We recommend installing an "empty" dotCMS version - this is done in the `ec2-create-data.tpl` file on the line that follows  
  `# Get empty dotcms project starter.zip`  
  For this to work, ensure that you have the empty `starter.zip` uploaded to the S3 bucket  - [dotCMS documentation - Installing with an Empty Site](https://dotcms.com/docs/latest/installing-with-an-empty-site) (See **dotCMS Dependencies** below for more information)
* `license-file-name` The license file to apply to your dotCMS environment - it must be uploaded to the `com-amwater-dotcms-software` S3 bucket
* `load-balancer-inbound-cidr` This defaults to `["0.0.0.0/0"]` - i.e. all traffic - If you are creating an environment that should be locked down (i.e. an Authoring environment or an Intranet environment) you may want to specify what traffic to allow into your dotCMS environment. In the example above it is configured to allow traffic from a contractor AmWater network and from the NAT Gateway Elastic IP around the VPC in which the environments are being created
  **IMPORTANT** - For push publishing/external applications to be able to reach your dotCMS site you *must* include the elastic IP in the list of allowed inbound CIDR blocks.
  
## Underlying details

### Terraform

Terraform files define a desired state of the infrastructure. When you run a Terraform file, Terraform compares the desired state to the current known state. Terraform only knows about the resources that it created or that you configure it to know about. This "knowledge" is stored in a state file and is specific to the environment. If you have separate production and QA environments, then each environment would only know about the resources for its environment.

### Structure

```none
+-- environments  
|  +--  dev2  
|  +--  multinode  
|  
+-- modules  
|  +--  dotcms-node  
|  +--  ec2  
|  +--  efs  
|  +--  elastisearch  
|  +--  password  
|  +--  rds  
|  +--  security-groups  
|  +--  rds-subnet-group
```  

The environments directory holds the `main.tf` and `init.sh` files necessary for creating a brand new environment or initializing a backend off of an existing `.tfstate` file, respectively.
The modules directory holds the information to configure a dotCMS node. It is split by component structure. The dotcms-node is the top level module that combines the others.

### Initialization

To create a stack, the backend must be initialized, and a variable file containing the necessary values must be created first.

To initialize the backend:

```bash
terraform init -backend-config="bucket=<bucket-name>" -backend-config="region=<bucket-region>" -backend-config="key=dotcms-amwater-<env>-<region>.tfstate"
```

where:

* `bucket-name` is the name of an existing AWS bucket where the Terraform state will be stored
* `bucket-region` is the AWS region in which this bucket was created (e.g. us-east-1) (not to be confused with the `aws-region` in the locals block of the `main.tf` file)
* `env` is the name of the environment to create (e.g. dev, test, etc.)  
* `region` is the AWS region where this environment will be created (this is just for information purposes)

### Details

The main.tf under your environment has the configuration for the dotCMS node(s). The main function of this file is to pass variables down to the `dotcms-node` module, which in turn passes variables down to the underlying modules.

### Notes

The structure is motivated by [Terraform Up & Running](https://www.terraformupandrunning.com).

### dotCMS Dependencies

#### License Packs

Each of the **dotcms-node** modules created by the script need a valid license. This is referenced in the `ec2-create-data.tpl` file under the `modules/ec2` directory. The command described as # Apply a cluster license copies the specified license file from an s3 bucket - defined by the `variable "license-software-s3-bucket"` in the `dotcms-node` module (if not specified, it will default to `com-amwater-dotcms-software`)

#### dotCMS Installation - Version

* The `ec2-create-data.tpl` is derived from the [dotCMS Basic AWS Installation Documentation](https://dotcms.com/docs/latest/basic-aws-installation)  
* The script is mostly self-explanatory - there are a few dependencies to be concerned about  
  * **License** - Each of the **dotcms-node** modules created by the script need a valid dotCMS license. This is referenced in the `ec2-create-data.tpl` file under the `modules/ec2` directory.  
  The command described as `# Apply a cluster license` copies the specified license file from an s3 bucket and relies on:
    * `variable "license-software-s3-bucket"` in the `dotcms-node` module (if not specified, it will default to `com-amwater-dotcms-software`) - this is where the license file needs to be uploaded  
    * `variable "license-file-name"` in the `dotcms-node` module - this is a required variable and should reference the appropriate license file in the s3 bucket  
  * **dotCMS Version** - The script's main purpose is to install dotCMS via a tar file  
  The command described as `# Get the tar file for the specified version from the s3 bucket` relies on:  
    * `variable "license-software-s3-bucket"` in the `dotcms-node` module (if not specified, it will default to `com-amwater-dotcms-software`)  
    * `variable "dotcms-version"` in the `dotcms-node` module - has a default version - where the file is named `dotcms_$DOTCMS_VERSION.tar.gz` and is present in the specified s3 bucket
  * **Empty starter.zip** - We have configured the script to [install with an *Empty Site*](https://dotcms.com/docs/latest/installing-with-an-empty-site) 
  The command described as `# Get empty dotcms project starter.zip` copies a file from your `license-software-s3-bucket` and expects it to be named `starter.zip`
  
* You must provision an instance profile with access to the s3 bucket specified by variable `license-software-s3-bucket` [Using Instance Profiles](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-ec2_instance-profiles.html) - `/modules/ec2/main.tf` creates resource `aws_launch_template` - this instance profile must be passed as variable, it defaults to the instance profile created for Sandbox AWS

### Troubleshooting after Creation  

We have observed an issue where the dotCMS install does not complete correctly on creation of an empty instance. To remedy this, ssh into each of the ec2 instances and run a `sudo systemctl stop dotcms` sequentially on each node, followed by a `sudo systemctl start dotcms` on each node, one after the other, in the same order that you brought them down. This should resolve any issues that might be observed after creation. The dotCMS documentation also has information on common troubleshooting.  
