locals {
  vpc-id            = "vpc-0699f2521432f8b22"
  subnet-ids        = ["subnet-094423e298c40fc2a", "subnet-09a27bac24fb72ed5", "subnet-0c660d66aba64656c"]
  public-key        = "../../dotcms-ec2-key.pub"
  env               = "dev"
  aws-region        = "eu-west-1"
}

terraform {
  backend "s3" {}
}

provider "aws" {
  region = local.aws-region
}

module "dev-node-password" {
  source                = "../../modules/password"
}

module "dev-node" {
  source                = "../../modules/dotcms-node"
  aws-region            = local.aws-region
  client-name           = "amwater-mysource"
  env                   = local.env
  license-file-name     = "dotCMSLicensePack1548706154412.zip"
  vpc-id                = local.vpc-id
  private-subnet-ids    = local.subnet-ids
  public-subnet-ids     = local.subnet-ids
  rds-user              = "dev_admin"
  rds-password          = module.dev-node-password.password
  rds-instance-class    = "db.t2.small"
  rds-allocated-storage = "8"
  public-key            = local.public-key
  ec2-instance-type     = "t3.medium"
  scaling-max           = "3"
  scaling-min           = "2"
  dotcms-version        = "5.1.5"
  rds-snapshot-id       = ""
//  rds-subnet-group-id   = "${module.db-subnet-group.subnet-group-id}"
}

