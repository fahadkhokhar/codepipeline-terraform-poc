#!/bin/bash
terraform init -backend-config="bucket=com-amwater-terraform-state" -backend-config="region=us-east-1" -backend-config="key=dotcms-amwater-dev.tfstate"