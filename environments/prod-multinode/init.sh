#!/bin/bash
terraform init -backend-config="bucket=com-amwater-terraform-state-prod" -backend-config="region=us-east-1" -backend-config="key=dotcms-amwater-prod-multinode-us-east-1.tfstate"