//Spun up in Sandbox - Virginia

locals {
  vpc-id              = "vpc-0d1565f8c08d440e0"
  public-subnet-ids   = ["subnet-07687565cb18868eb", "subnet-02bce9d53d734c5aa", "subnet-0cf103dbf8e670f13"]
  private-subnet-ids  = ["subnet-08dc2ec3ec0897902", "subnet-0693e7518614707be", "subnet-0975838afb9b7c307"]
  public-key          = "../../dotcms-ec2-key.pub"
  env                 = "dev"
  aws-region          = "us-east-1"
}

terraform {
  backend "s3" {}
}

provider "aws" {
  region = local.aws-region
}

module "db-subnet-group" {
  source                      = "../../modules/rds-subnet-group"
  private-subnet-ids          = local.private-subnet-ids
  env                         = local.env
}

module "dev-node-password" {
  source            = "../../modules/password"
}

module "dev-node" {
  source                      = "../../modules/dotcms-node"
  aws-region                  = local.aws-region
  client-name                 = "mysource-headful"
  env                         = local.env
  load-balancer-inbound-cidr  = ["12.27.32.30/32", "161.69.122.12/32"]
  license-file-name           = "dotCMSLicensePack1548706154412.zip"
  vpc-id                      = local.vpc-id
  private-subnet-ids          = local.private-subnet-ids
  public-subnet-ids           = local.public-subnet-ids
  ssh-security-group          = "sg-00b04e1420312e50d"
  rds-user                    = "dotcms"
  rds-password                = module.dev-node-password.password
  rds-instance-class          = "db.t2.small"
  rds-allocated-storage       = "20"
  public-key                  = local.public-key
  ec2-instance-type           = "m5a.large"
  scaling-max                 = "3"
  scaling-min                 = "2"
  dotcms-version              = "5.1.5"
  rds-snapshot-id             = "arn:aws:rds:us-east-1:541676414377:snapshot:mysource-prod-migration"
  rds-subnet-group-id         = "${module.db-subnet-group.subnet-group-id}"
}

