// These are values that are likely to be the same for all nodes in an installation, even as they are likely to be
//  different for different installations.

//Virginia - Running with AmWater - Amwater platform vpc
locals {
  vpc-id                      = "vpc-00fe7a267b76f6697"
  public-subnet-ids           = ["subnet-0adf29c90214ea15b", "subnet-01fc19fb69721da9e", "subnet-03c1ff4ae097e6a6b"]
  private-subnet-ids          = ["subnet-0f5f6c7f978d5a4dc", "subnet-0b3f0ba2beaddbd2a", "subnet-0405e508723defedb"]
  public-key                  = "../../dotcms-ec2-key.pub"
  env                         = "nonprod"
  aws-region                  = "us-east-1"
  dotcms-version              = "5.1.5"
  license-software-s3-bucket  = "com-amwater-dotcms-software-np"
  ec2-launch-arn              = "arn:aws:iam::462393762422:instance-profile/ec2-dotcms"
  autoscaling-linked-role-arn = "arn:aws:iam::462393762422:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
  ssh-security-group-id       = "sg-0b75cccfe6d2e8543"
}

terraform {
  backend "s3" {}
}

provider "aws" {
  region = local.aws-region
}

module "db-subnet-group" {
  source                      = "../../modules/rds-subnet-group"
  private-subnet-ids          = local.private-subnet-ids
  env                         = local.env
}

module "authoring-password" {
  source                      = "../../modules/password"
}

module "authoring-node" {
  source                      = "../../modules/dotcms-node"
  aws-region                  = local.aws-region
  client-name                 = "authoring"
  env                         = local.env
  license-file-name           = "dotCMSLicensePack1561555681053.zip"
  license-software-s3-bucket  = local.license-software-s3-bucket
  vpc-id                      = local.vpc-id
  load-balancer-inbound-cidr  = ["12.27.32.30/32", "161.69.122.12/32", "35.175.3.10/32"]
  private-subnet-ids          = local.private-subnet-ids
  public-subnet-ids           = local.public-subnet-ids
  rds-user                    = "dev_admin"
  rds-password                = module.authoring-password.password
  rds-instance-class          = "db.t2.small"
  rds-allocated-storage       = "20"
  public-key                  = local.public-key
  ec2-launch-arn              = local.ec2-launch-arn
  ec2-instance-type           = "t3.medium"
  scaling-max                 = "3"
  scaling-min                 = "2"
  autoscaling-linked-role-arn = local.autoscaling-linked-role-arn
  ssh-security-group          = local.ssh-security-group-id
  dotcms-version              = local.dotcms-version
  rds-snapshot-id             = "arn:aws:rds:us-east-1:719620547956:snapshot:ir-dotcms-091619"
  rds-subnet-group-id         = "${module.db-subnet-group.subnet-group-id}"
}


module "regulated-password" {
  source                      = "../../modules/password"
}

module "regulated-node" {
  source                      = "../../modules/dotcms-node"
  aws-region                  = local.aws-region
  client-name                 = "regulated"
  env                         = local.env
  vpc-id                      = local.vpc-id
  load-balancer-inbound-cidr  = ["12.27.32.30/32", "161.69.122.12/32", "35.175.3.10/32"]
  public-subnet-ids           = local.public-subnet-ids
  private-subnet-ids          = local.private-subnet-ids
  rds-user                    = "dev_admin"
  rds-password                = "${module.regulated-password.password}"
  rds-instance-class          = "db.t3.large"
  rds-allocated-storage       = "20"
  public-key                  = local.public-key
  ec2-instance-type           = "t3.medium"
  ec2-launch-arn              = local.ec2-launch-arn
  license-file-name           = "dotCMSLicensePack1561555701486.zip"
  license-software-s3-bucket  = local.license-software-s3-bucket
  scaling-max                 = "3"
  scaling-min                 = "2"
  autoscaling-linked-role-arn = local.autoscaling-linked-role-arn
  ssh-security-group          = local.ssh-security-group-id
  dotcms-version              = local.dotcms-version
  rds-snapshot-id             = "arn:aws:rds:us-east-1:719620547956:snapshot:ir-dotcms-091619"
  rds-subnet-group-id         = "${module.db-subnet-group.subnet-group-id}"
}





//module "nonregulated-password" {
//  source                      = "../../modules/password"
//}
//
//
//module "nonregulated-node" {
//  source                      = "../../modules/dotcms-node"
//  aws-region                  = local.aws-region
//  client-name                 = "nonregulated"
//  env                         = local.env
//  license-file-name           = "dotCMSLicensePack1561555701486.zip"
//  license-software-s3-bucket  = local.license-software-s3-bucket
//  vpc-id                      = local.vpc-id
//  subnet-ids                  = local.subnet-ids
//  rds-user                    = "dev_admin"
//  rds-password                = module.nonregulated-password.password
//  rds-instance-class          = "db.t2.small"
//  rds-allocated-storage       = "8"
//  public-key                  = local.public-key
//  ec2-instance-type           = "t3.medium"
//  scaling-max                 = "3"
//  scaling-min                 = "2"
//  dotcms-version              = local.dotcms-version
//  rds-snapshot-id             = ""
//  rds-subnet-group-id         = "${module.db-subnet-group.subnet-group-id}"
//}
//
//module "intranet-password" {
//  source                      = "../../modules/password"
//}
//
//module "intranet-node" {
//  source                      = "../../modules/dotcms-node"
//  aws-region                  = local.aws-region
//  client-name                 = "intranet"
//  env                         = local.env
//  public-key                  = local.public-key
//  ec2-instance-type           = "t3.medium"
//  license-file-name           = "dotCMSLicensePack1561555695083.zip"
//  license-software-s3-bucket  = local.license-software-s3-bucket
//  vpc-id                      = local.vpc-id
//  subnet-ids                  = local.subnet-ids
//  rds-user                    = "dev_admin"
//  rds-password                = module.intranet-password.password
//  rds-instance-class          = "db.t2.small"
//  rds-allocated-storage       = "8"
//  scaling-max                 = "3"
//  scaling-min                 = "2"
//  dotcms-version              = local.dotcms-version
//  rds-snapshot-id             = ""
//  rds-subnet-group-id         = "${module.db-subnet-group.subnet-group-id}"
//}
//
