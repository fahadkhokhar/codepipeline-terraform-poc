# Restoring from Backup/Migrating an Existing Site

The purpose of this document is to provide basic guidance on how to move the necessary assets from an existing dotCMS server to a new instance running the same version of dotCMS

## Callouts and Assumptions

**Pre-Requisite**: Make sure that you have a good understanding of the terraform scripts - start with ***Creating a Brand New Environment*** in the `README`

You must maintain the same permissions, modes, and file structures on files that you are moving - the easiest way to do this is via `tar` and `aws s3 cp` or `rsync` commands.

You can read more about the dotCMS requirements [here](https://dotcms.com/docs/latest/backup-and-restore)

This guide is intended to give more specific commands with the following assumptions:

* You have a dotCMS running on an AWS environment
* You have ssh access into the ec2 instances (both in the *source* and *destination* environments)
* You will be spinning up the *destination* dotCMS environment using the terraform script and an RDS snapshot from the *source* dotCMS environment.

## Terraforming from an RDS Snapshot

You will need to create a snapshot of the RDS instance(s) you wish to migrate. This is covered in detail by the [AWS documentation](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_CreateSnapshot.html). If you are trying to create the environment in a different region or with a different AWS account, ensure that the snapshot has been shared with the appropriate region/account.

Run the terraform script with the identifier referenced as the variable `rds-snapshot-id` in your `main.tf` file.

## Package and move all necessary files from the Source Server

Be sure that you have stopped all dotcms services via `systemctl stop dotcms`

### Packaging the files

We are interested in copying all of the assets needed for a full migration (as defined by the [dotCMS documentation](https://dotcms.com/docs/latest/backup-and-restore))

In order to make accomplish this in fewer commands/move fewer files, it is recommended to archive the files into a tar file before copying from the source server

1. ssh into the source ec2
2. Assume the dotcms role (if you did not ssh in as the dotcms user)
3. Run the following commands to package up the files of interest  
  `tar -cvzf data_dir_[date].tar.gz /opt/dotcms/data`  
  Where:  
    `-cvzf`  

    * c = creates new (in the current directory)
    * v = verbose  
    * z = compress to gzip  
    * f = specify filename  
  
    `data_dir_[date].tar.gz` - Represents the name of the resulting tar file  
    `/opt/dotcms/data` - Represents the directory that will recursively be copied into the tar file  
    [More information on the tar command](http://man7.org/linux/man-pages/man1/tar.1.html)
  
* Note: Due to the typical size of the `/opt/dotcms/data` directory, we recommend tarring up the files on a location on the EFS mount (mounted at `/opt/dotcms/data/assets`) - example commands after sshing into the ec2 instance:

```bash
mkdir /opt/dotcms/data/assets/migration
tar -cvzf /opt/dotcms/data/assets/migration/data_dir_[identifier].tar.gz /opt/dotcms/data
```

### Moving the files

The most efficient way to move these compressed files is to upload them to an S3 bucket via `aws s3 cp` commands - this requires an instance profile to be attached to the _source_ ec2 instance from which you are trying to migrate. [`aws s3 cp` command reference](https://docs.aws.amazon.com/cli/latest/reference/s3/cp.html)

You can also use a traditional `rsync` or `scp` command to copy to your local machine and then to the destination server, but it will not be as performant. [More information on the `rsync` command](http://man7.org/linux/man-pages/man1/rsync.1.html)

### Example Script - Source Server

```bash  
sudo su dotcms
cd /
mkdir /opt/dotcms/data/assets/migration
# Be sure to execute this from the root directory to maintain the path structure
tar -cvzf /opt/dotcms/data/assets/migration/data_dir_[identifier].tar.gz /opt/dotcms/data --exclude=/opt/dotcms/data/assets/migration
cd /opt/dotcms/data/assets/migration
# Copy source file to the s3 bucket
aws s3 data_dir_[identifier].tar.gz s3://com-amwater-dotcms-backups-[env]
cd ..
# Cleanup
rm -rf migration/
exit
```

## Move and unpackage all files to the Destination Server

We will now follow a similar procedure on the destination server, but in reverse. Be sure that you have stopped all dotcms services via `systemctl stop dotcms`

### Moving all files onto the destination server

The most efficient way to get the compressed `/opt/dotcms/data` directory onto the destination server is via the `aws s3 cp` command. This requires an instance profile to be attached to the ec2 instance that has access to the s3 bucket to which the source tar.gz file was uploaded. [`aws s3 cp` command reference](https://docs.aws.amazon.com/cli/latest/reference/s3/cp.html)

You can also use a traditional `rsync` or `scp` command from a local machine to the destination server. [More information on the `rsync` command](http://man7.org/linux/man-pages/man1/rsync.1.html)

### Unpackaging all files onto the destination server

With the compressed data.tar.gz file on the destination server, the next step is to uncompress the file via `tar -xvf [migrated_file].tar.gz -C /`

Option `-C` allows you to specify the directory into which the files will be untarred - if you have preserved all of the directories, then specifying the root here should be adequate (alternatively you can run the command from `/` and leave off the option)

### Example Script - Destination Server

```bash  
sudo su dotcms
cd /
mkdir /opt/dotcms/data/assets/migration
#Copy source file from the s3 bucket
aws s3 s3://com-amwater-dotcms-backups-[env]/data_dir_[identifier].tar.gz  /opt/dotcms/data/assets/migration
# Be sure to execute this from the root directory to maintain the path structure
tar -xvf /opt/dotcms/data/assets/migration/data_dir_[identifier].tar.gz
# Cleanup
rm -rf migration/
exit
```

## Additional cleanup and starting up the destination server(s)

### Database Cleanup

If restoring from an RDS snapshot, I have observed behavior that preserves the original (source) user and ownership on the dotcms database table. To correct this, do the following:

From one of the ec2 instances, connect to the database. The database connection details will be in the `context.xml` file (located at) `/opt/dotcms/wwwroot/current/dotserver/tomcat-x.x.x/webapps/ROOT/META-INF`

Assuming that the source username is `dev_admin` and the destination username is `prod_admin` - execute the following commands:

First connect with `dev_admin` as the user

``` sql
CREATE ROLE prod_admin WITH PASSWORD (password_from_context_xml);
ALTER ROLE prod_admin WITH LOGIN;
GRANT dev_admin TO prod_admin;
ALTER DATABASE dotcms OWNER TO prod_admin;
```

To verify - close the connection and try reconnecting to dbname=dotcms with user=prod_admin

Additionally, there are some database tables that contain site specific information. To avoid any misbehavior on migration, connect to the database from an ec2 instance. This will require you to install postgresql (via `sudo yum install postgresql`) and connect via a `psql "port=5432 host= dbname= user=`, where the database config will match what is stored in the `context.xml` file located at `/opt/dotcms/wwwroot/current/dotserver/tomcat-x.x.x/webapps/ROOT/META-INF` (alternatively, this information can be retrieved from the .tfstate file in the s3 bucket).

Once connected to the database, run the following commands:

``` sql
DELETE FROM sitelic;
DELETE FROM cluster_server_uptime;
DELETE FROM cluster_server_action;
DELETE FROM cluster_server;
```

### Starting dotCMS services

Bring back up the dotCMS node(s) one by one. You can follow the dotCMS log output at the default location of `/opt/dotcms/wwwroot/current/dotserver/tomcat-x.x.x/logs/catalina.out`

### Applying licenses

If you want to apply a different license pack than your destination server, follow these steps:

1. Bring down dotCMS services on all nodes
2. Delete the contents of the `/opt/dotcms/data/dotsecure/license` directory
3. Bring back up _one_ dotCMS node
4. Login to the dotCMS admin and apply the license pack
5. Bring back up the remaining dotCMS node(s) one by one

More information on [license packs](https://dotcms.com/docs/latest/license-management) and [clustering configuration](https://dotcms.com/docs/latest/auto-clustering-configuration)

### Troubleshooting

You may encounter some errors related to the indices - if this occurs:

1. Bring down dotCMS services on all nodes
2. Delete the contents of the /esdata folder (located at `/opt/dotcms/data/dotsecure/esdata`)
3. Delete the contents of the /h22cache folder (located at `/opt/dotcms/data/dotsecure/h22cache`)
4. Bring back up dotCMS services (one node at a time)
5. Login to the dotCMS Admin and do a complete [reindex](https://dotcms.com/docs/latest/reindexing-content).
