resource "random_string" "password" {
  length = 16
//  Turned special characters off, because I was seeing some issues with connecting to database. Special characters
//  in the password seem like the most likely issue.
  special = false
  min_lower = 1
  min_numeric = 1
//  min_special = 1
  min_upper = 1
//  override_special = "!#$%&*()-_=+[]{}<>:?"
}