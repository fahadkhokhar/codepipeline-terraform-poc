output "rds-instance-id" {
  value = "${aws_db_instance.rds-instance.id}"
}

output "rds-instance-url" {
  value = "${aws_db_instance.rds-instance.address}"
}

output "rds-instance-address" {
  value = "${aws_db_instance.rds-instance.endpoint}"
}

output "rds-instance-schema" {
  value = "${aws_db_instance.rds-instance.name}"
}
