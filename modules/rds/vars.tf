variable aws-region {
  type        = "string"
  description = "The AWS region in which the resource will be created"
  default     = "eu-central-1"
}

variable "client-name" {
  type        = "string"
  description = "Name of the client to for this resource will be utilized"
  default     = "amwater-dotcms"
}

variable "env" {
  type        = "string"
  description = "Name of environment to be created for the resource"
  default     = "dev"
}

variable "rds-schema" {
  type        = "string"
  description = "The name of the schema in the RDS"
}

variable "rds-user" {
  type        = "string"
  description = "The user name to use when connecting to the RDS"
  default     = "admin"
}

variable "rds-password" {
  type        = "string"
  description = "The password to use when connecting to the RDS"
}

variable "rds-port" {
  type        = "string"
  description = "The port that the RDS listens on"
  default     = "5432"
}

variable "vpc-id" {
  type        = "string"
  description = "Id of the VPC in which the resources will be created"
}

variable "rds-security-group-id" {
  type        = "string"
  description = "Security group id that will apply to the RDS"
}

variable "rds-instance-class" {
  type        = "string"
  description = "The instance class for the database"
}

variable "rds-snapshot-id" {
  type        = "string"
  description = "The instance class for the database"
  default     = ""
}

variable "allocated_storage" {
  type        = "string"
  description = "The allocated size of the database in GB"
  default     = "20"

}

variable "subnet-group-id" {
  type = "string"
  description = "ids"
}