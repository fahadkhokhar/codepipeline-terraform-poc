resource "aws_db_instance" "rds-instance" {
  identifier                          = "${var.client-name}-${var.env}-rds"
  allocated_storage                   = "${var.allocated_storage}"
  max_allocated_storage               = 1000
  auto_minor_version_upgrade          = true
  deletion_protection                 = false
  engine                              = "postgres"
  engine_version                      = "10.6"
  iam_database_authentication_enabled = false
  instance_class                      = "${var.rds-instance-class}"
  name                                = "${var.rds-schema}"
  username                            = "${var.rds-user}"
  password                            = "${var.rds-password}"
  port                                = "${var.rds-port}"
  vpc_security_group_ids              = ["${var.rds-security-group-id}"]
  backup_retention_period             = 7
  skip_final_snapshot                 = true
  multi_az                            = true
  apply_immediately                   = true
  snapshot_identifier                 = "${var.rds-snapshot-id}"
  db_subnet_group_name                = "${var.subnet-group-id}"

  tags = {
    Client = "${var.client-name}"
    Env    = "${var.env}"
  }
}
