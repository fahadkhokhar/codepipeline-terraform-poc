resource "aws_security_group" "load-balancer-security-group" {
  name        = "${var.client-name}-${var.env}-lb-security"
  description = "Allow inbound traffic on HTTP/HTTPS to the load balancer from any IP"
  vpc_id      = "${var.vpc-id}"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "TCP"

    cidr_blocks = var.load-balancer-inbound-cidr
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "TCP"

    cidr_blocks = var.load-balancer-inbound-cidr
}



  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "ec2-instance-security-group" {
  name        = "${var.client-name}-${var.env}-ec2-security"
  description = "Only allow inbound traffic from the load balancer to the EC2 instance"
  vpc_id      = "${var.vpc-id}"

  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "TCP"

    security_groups = [
      "${aws_security_group.load-balancer-security-group.id}",
    ]
  }

  ingress {
    from_port = 8443
    to_port   = 8443
    protocol  = "TCP"

    security_groups = [
      "${aws_security_group.load-balancer-security-group.id}",
    ]
  }

  ingress {
    from_port = 22
    protocol  = "TCP"
    to_port   = 22

    security_groups = [
      "${var.ssh-security-group}"
    ]

  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "ec2-instance-cluster-security-group" {
  name        = "${var.client-name}-${var.env}-ec2-cluster-security"
  description = "Allow cluster traffic between all the EC2 members"
  vpc_id      = "${var.vpc-id}"

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "TCP"

    security_groups = [
      "${aws_security_group.ec2-instance-security-group.id}",
    ]
  }
}

resource "aws_security_group" "efs-security-group" {
  name        = "${var.client-name}-${var.env}-efs-security"
  description = "Only allow inbound traffic from the EC2 instances to EFS"
  vpc_id      = "${var.vpc-id}"

  ingress {
    from_port = 2049
    to_port   = 2049
    protocol  = "TCP"

    security_groups = [
      "${aws_security_group.ec2-instance-security-group.id}",
    ]
  }
}

resource "aws_security_group" "rds-security-group" {
  name        = "${var.client-name}-${var.env}-rds-security"
  description = "Only allow inbound traffic from the EC2 instances"
  vpc_id      = "${var.vpc-id}"

  ingress {
    from_port = "${var.rds-port}"
    to_port   = "${var.rds-port}"
    protocol  = "TCP"

    security_groups = [
      "${aws_security_group.ec2-instance-security-group.id}",
    ]
  }
}
