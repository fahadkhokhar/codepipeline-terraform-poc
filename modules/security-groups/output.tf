output "load-balancer-security-group-id" {
  value = "${aws_security_group.load-balancer-security-group.id}"
}

output "ec2-instance-security-group-id" {
  value = "${aws_security_group.ec2-instance-security-group.id}"
}

output "ec2-cluser-security-group-id" {
  value = "${aws_security_group.ec2-instance-cluster-security-group.id}"
}

output "efs-security-group-id" {
  value = "${aws_security_group.efs-security-group.id}"
}

output "rds-security-group-id" {
  value = "${aws_security_group.rds-security-group.id}"
}
