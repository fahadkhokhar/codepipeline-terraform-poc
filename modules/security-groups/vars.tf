variable aws-region {
  type        = "string"
  description = "The AWS region in which the resource will be created"
  default     = "eu-central-1"
}

variable "client-name" {
  type        = "string"
  description = "Name of the client to for this resource will be utilized"
  default     = "amwater-dotcms"
}

variable "env" {
  type        = "string"
  description = "Name of environment to be created for the resource"
  default     = "dev"
}

variable "load-balancer-inbound-cidr" {
  type        = "list"
  description = "The CIDR blocks to allow inbound traffic to the load balancer - defaults to any"
  default     = [
    "0.0.0.0/0"
  ]
}

variable "vpc-id" {
  type        = "string"
  description = "Id of the VPC in which the resources will be created"
}

variable "rds-port" {
  type        = "string"
  description = "The port that the RDS listens on"
}

variable "ssh-security-group" {
  type        = "string"
  description = "Additional security group to allow ssh from"
  default     = ""
}
