variable "private-subnet-ids" {
  type        = "list"
  description = "Ids of the subnets to put the RDS instances in"
}

variable "env" {
  type        = "string"
  description = "Name of environment to be created for the resource"
  default     = "dev"
}