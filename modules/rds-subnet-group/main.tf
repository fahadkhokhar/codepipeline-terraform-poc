resource "aws_db_subnet_group" "default" {
  name       = "${var.env}-subnet-group"
  subnet_ids = var.private-subnet-ids

  tags = {
    Name = "DB subnet group"
  }
}