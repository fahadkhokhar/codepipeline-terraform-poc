// https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html
// https://www.terraform.io/docs/providers/aws/d/ami.html

data "aws_ami" "amazon-linux-2" {
  owners = ["amazon"]
  most_recent      = true
  name_regex       = "amzn2-ami-hvm-2.0.(.*)-x86_64-gp2"

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

//  filter {
//    name   = "name"
//    values = ["amzn2-ami-hvm-2.0.*"]
//  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.20190508-x86_64-gp2"]
  }



//  filter {
//    name = "platform"
//    values = ["Amazon Linux"]
//  }

  filter {
    name = "state"
    values = ["available"]
  }

//  ^amzn2-ami-hvm-2.0.????????-x86_64-gp2$
}
//
//data "local_file" "license-file" {
//  filename = "${var.license-file-path}"
//}

data "template_file" "ec2-user-data" {
  template = "${file("${path.module}/ec2-create-data.tpl")}"

  vars = {
    dotcms-version          = "${var.dotcms-version}"
    efs-id                  = "${var.efs-id}"
    rds-url                 = "${var.rds-url}"
    rds-schema              = "${var.rds-schema}"
    rds-user                = "${var.rds-user}"
    rds-password            = "${var.rds-password}"
// Here so I can leave the single license mechanism in for now
    license-key             = ""
//    license-key    = "${data.local_file.license-file.content}"

// Expects to find this license in the specified S3 bucket
    license-software-bucket = "${var.license-software-bucket}"
    license-name            = "${var.license-file-name}"
  }
}
