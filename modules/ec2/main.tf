resource "aws_launch_template" "ec2-launch-template" {
  name        = "${var.client-name}-${var.env}-ec2-launch-template"
  description = "Launch template for dotCMS EC2 instances from ${data.aws_ami.amazon-linux-2.id}"

  image_id = data.aws_ami.amazon-linux-2.id

  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      volume_size = 8
    }
  }

  disable_api_termination = false

  ebs_optimized = true

  instance_type = var.ec2-instance-type

  iam_instance_profile {
    arn = "${var.ec2-launch-arn}"
  }

  key_name = "${var.key-name}"

  monitoring {
    enabled = true
  }

  vpc_security_group_ids = [
    "${var.ec2-instance-security-group-id}",
    "${var.ec2-cluster-security-group-id}",
  ]

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "${var.client-name}-${var.env}-ec2"
      Env  = "${var.env}"
    }
  }

  user_data = "${base64encode(data.template_file.ec2-user-data.rendered)}"
}

resource "aws_autoscaling_policy" "ec2-scaling-policy" {
  autoscaling_group_name = aws_autoscaling_group.asg.name
  name                   = "${var.client-name}-${var.env}-scaling-policy"
  policy_type            = "SimpleScaling"
  cooldown               = 600
  scaling_adjustment     = 3
  adjustment_type        = "ChangeInCapacity"
}

// Possible health check?
// {applicationLoadBalancerDNS}/api/v1/system-status
resource "aws_autoscaling_group" "asg" {
  max_size            = var.max_size
  min_size            = var.min_size
  vpc_zone_identifier = var.private-subnet-ids

  service_linked_role_arn = "${var.autoscaling-linked-role-arn}"
  target_group_arns = ["${aws_alb_target_group.alb-target-group.arn}"]
  default_cooldown  = 600

  launch_template {
    id      = "${aws_launch_template.ec2-launch-template.id}"
    version = "$Latest"
  }

  depends_on = [var.dependencies]
}

resource "aws_alb" "alb" {
  name                       = "${var.client-name}-${var.env}-alb"
  security_groups            = [var.load-balancer-security-group-id]
  load_balancer_type         = "application"
  enable_deletion_protection = false
  subnets                    = var.public-subnet-ids

  tags = {
    Name = "${var.client-name}-${var.env}-alb"
    Env  = var.env
  }
}

resource "aws_alb_target_group" "alb-target-group" {
  name        = "${var.client-name}-${var.env}-tg"
  vpc_id      = var.vpc-id
  port        = 8080
  protocol    = "HTTP"
  slow_start  = 300
  target_type = "instance"

  tags = {
    Name = "${var.client-name}-${var.env}-tg"
    Env  = var.env
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 3600
  }
}

resource "aws_alb_listener" "alb-listener" {
  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb-target-group.arn
  }

  load_balancer_arn = aws_alb.alb.arn
  port              = 80
}

resource "aws_lb_listener_rule" "block" {
  listener_arn = "${aws_alb_listener.alb-listener.arn}"
  priority     = 100
  count = length(var.block_paths) > 0 ? 1 : 0

  action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Forbidden"
      status_code  = "403"
    }
  }

  condition {
    field  = "path-pattern"
    values = var.block_paths
  }
}