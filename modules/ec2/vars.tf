variable aws-region {
  type        = "string"
  description = "The AWS region in which the resource will be created"
  default     = "eu-central-1"
}

variable "client-name" {
  type        = "string"
  description = "Name of the client to for this resource will be utilized"
  default     = "amwater-dotcms"
}

variable "env" {
  type        = "string"
  description = "Name of environment to be created for the resource"
  default     = "dev"
}

variable "vpc-id" {
  type        = "string"
  description = "Id of the VPC in which the resources will be created"
}

variable "private-subnet-ids" {
  type        = "list"
  description = "Ids of the subnets to put the EC2 instances in"
}

variable "public-subnet-ids" {
  type        = "list"
  description = "Ids of the subnets to put the public facing loadbalancer"
}

variable "efs-id" {
  type        = "string"
  description = "Id of the Elastic File system to mount for the assets folder"
}

variable "rds-url" {
  type        = "string"
  description = "The DNS name of the RDS"
}

variable "rds-schema" {
  type        = "string"
  description = "The name of the schema in the RDS"
}

variable "rds-user" {
  type        = "string"
  description = "The user name to use when connecting to the RDS"
}

variable "rds-password" {
  type        = "string"
  description = "The password to use when connecting to the RDS"
}

variable "license-file-name" {
  type        = "string"
  description = "The name of the license file to use - must be stored in the 'license-software-bucket' that is specified"
}

variable "license-software-bucket" {
  type        = "string"
  description = "The s3 bucket that holds the license files and dotCMS software zip files"
  default     = "com-amwater-dotcms-software"
}

variable "load-balancer-security-group-id" {
  type        = "string"
  description = "The security group id for the policy governing the load balancer"
}

variable "ec2-instance-security-group-id" {
  type        = "string"
  description = "The security group id for the policy governing the ec2 instance"
}

variable "ec2-cluster-security-group-id" {
  type        = "string"
  description = "The security group id for the policy governing the ec2 cluster instances"
}

variable "ec2-launch-arn" {
  type = "string"
  description = "The arn for the ec2 launch template"
  default = "arn:aws:iam::541676414377:instance-profile/ec2-dotcms"
}

variable "autoscaling-linked-role-arn" {
  type = "string"
  description = "The arn for the autoscaling group"
  default = "arn:aws:iam::541676414377:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
}

variable "key-name" {
  type        = "string"
  description = "The name of the EC2 key to be used to ssh in to an instance"
}

variable "dotcms-version" {
  type        = "string"
  description = "The version of dotCMS to pull from the S3 bucket, com-amwater-dotcms-software. The expected name is dotcms_DOTCMS-VERSION.tar.gz"
  default     = "5.1.6"
}

variable "ec2-instance-type" {
  type = "string"
  description = "The instance type for the EC2"
}

variable "max_size" {
  type        = "string"
  description = "The maximum number of EC2 instances in the scaling groups"
  default     = "1"
}
variable "min_size" {
  type        = "string"
  description = "The minimum number of EC2 instances in the scaling groups"
  default     = "1"
}

variable "block_paths" {
  type = "list"
  description = "A list of paths to block"
  default = []
}

variable dependencies {
  type        = "list"
  description = "Any dependencies for resource creation"
  default     = []
}
