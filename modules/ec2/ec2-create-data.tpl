#!/bin/bash

# Update the system with any new package versions, and security fixes
yum update -y

yum install -y java-1.8.0-openjdk-headless.x86_64
yum install -y amazon-efs-utils

# Create a dotcms system service that will allow dotCMS to start automatically on reboot
sh -c  "cat >/usr/lib/systemd/system/dotcms.service <<EOL
[Unit]
Description=dotCMS Server Process

[Service]
User=dotcms
Type=forking
WorkingDirectory=/opt/dotcms/wwwroot/current/
ExecStart=/opt/dotcms/wwwroot/current/bin/startup.sh
ExecStop=/opt/dotcms/wwwroot/current/bin/shutdown.sh
TimeoutSec=30
Restart=on-failure
RestartSec=30
StartLimitInterval=350
StartLimitBurst=10

[Install]
WantedBy=multi-user.target

EOL
"

systemctl enable dotcms

# Setup the dotcms user and app dirs
mkdir -p /opt/dotcms
mkdir -p /opt/dotcms/data/assets
mkdir -p /opt/dotcms/logs

# Add the EFS mount point for the assets directory to the fstab file to auto mount on reboot, and manually mount for the first time
sudo sh -c "echo '${efs-id}:/	/opt/dotcms/data/assets 	efs 	defaults,_netdev 0 0' >> /etc/fstab"
mount -t efs ${efs-id}:/ /opt/dotcms/data/assets

# Create the dotcms user
adduser -d /opt/dotcms dotcms

#Set the dotcms version and install directory based on the specified dotCMS version
DOTCMS_VERSION=${dotcms-version}

INSTALL_DIR=/opt/dotcms/wwwroot/$DOTCMS_VERSION

mkdir -p $INSTALL_DIR
ln -s $INSTALL_DIR /opt/dotcms/wwwroot/current
cd /opt/dotcms/wwwroot/current

# Get the tar file for the specified version from the s3 bucket
TAR_FILE=dotcms_$DOTCMS_VERSION.tar.gz

aws s3 cp s3://${license-software-bucket}/$TAR_FILE .
tar -zxf $TAR_FILE

rm $TAR_FILE

# Determine the Tomcat version bundled with dotCMS
TOMCAT_VERSION=`ls -l /opt/dotcms/wwwroot/current/dotserver/ | grep -Po 'tomcat-\K.*?$'`

# Set up the config plugin
mkdir -p plugins/com.dotcms.config/ROOT/dotserver/tomcat-$TOMCAT_VERSION/webapps/ROOT/META-INF
cp dotserver/tomcat-$TOMCAT_VERSION/webapps/ROOT/META-INF/context.xml plugins/com.dotcms.config/ROOT/dotserver/tomcat-$TOMCAT_VERSION/webapps/ROOT/META-INF/

# Update JVM memory to start with 4GB instead of 1GB
mkdir -p plugins/com.dotcms.config/ROOT/bin
cp bin/startup.sh plugins/com.dotcms.config/ROOT/bin/

# set your java memory, in this case changing 1G to 4G
sed -i 's/Xmx1G/Xmx4G/g' plugins/com.dotcms.config/ROOT/bin/startup.sh

## Create a context.xml for database and email jndi connections
sh -c  "cat > /tmp/context.xml << EOL
<Context path=\"/opt/dotcms/wwwroot/current/dotserver/tomcat-$TOMCAT_VERSION/webapps/ROOT\" docBase=\"/opt/dotcms/wwwroot/current/dotserver/tomcat-$TOMCAT_VERSION/webapps/ROOT\" reloadable=\"false\" crossContext=\"true\">
<Resources allowLinking=\"true\" />
<Loader delegate=\"true\"/>
<Resource name=\"mail/MailSession\" auth=\"Container\" type=\"javax.mail.Session\" mail.smtp.host=\"localhost\" />
<Resource name=\"jdbc/dotCMSPool\" auth=\"Container\"
	type=\"javax.sql.DataSource\"
	factory=\"org.apache.tomcat.jdbc.pool.DataSourceFactory\"
	driverClassName=\"org.postgresql.Driver\"
	url=\"jdbc:postgresql://${rds-url}/${rds-schema}\"
	username=\"${rds-user}\" password=\"${rds-password}\" maxActive=\"60\" maxIdle=\"10\" maxWait=\"60000\"
	removeAbandoned=\"true\" removeAbandonedTimeout=\"60\" logAbandoned=\"true\"
	timeBetweenEvictionRunsMillis=\"30000\" validationQuery=\"SELECT 1\" testWhileIdle=\"true\"
	abandonWhenPercentageFull=\"50\"/>
</Context>
EOL"

mv /tmp/context.xml plugins/com.dotcms.config/ROOT/dotserver/tomcat-$TOMCAT_VERSION/webapps/ROOT/META-INF/context.xml

# Set up properties override for dotCMS
echo "DYNAMIC_CONTENT_PATH=/opt/dotcms/data/dotsecure" >> plugins/com.dotcms.config/conf/dotmarketing-config-ext.properties
echo "ASSET_REAL_PATH=/opt/dotcms/data/assets" >> plugins/com.dotcms.config/conf/dotmarketing-config-ext.properties
echo "AUTOWIRE_CLUSTER_TRANSPORT=true" >> plugins/com.dotcms.config/conf/dotcms-config-cluster-ext.properties
echo "AUTOWIRE_CLUSTER_ES=true" >> plugins/com.dotcms.config/conf/dotcms-config-cluster-ext.properties
echo "TAIL_LOG_LOG_FOLDER=/opt/dotcms/wwwroot/current/dotserver/tomcat-$TOMCAT_VERSION/logs" >> plugins/com.dotcms.config/conf/dotmarketing-config-ext.properties

# Apply a single license file
#touch /opt/dotcms/data/dotsecure/license/license.dat
#echo "${license-key}" >> /opt/dotcms/data/dotsecure/license/license.dat

# Get empty dotcms project starter.zip
aws s3 cp s3://${license-software-bucket}/starter.zip /opt/dotcms/wwwroot/current/dotserver/tomcat-$TOMCAT_VERSION/webapps/ROOT/starter.zip

# Apply a cluster license
aws s3 cp s3://${license-software-bucket}/${license-name} /opt/dotcms/data/assets/license.zip

# deploy your repeatable config plugin (you should carry this over to new versions when you upgrade)
./bin/deploy-plugins.sh

# touch out so you can tail it
touch /opt/dotcms/wwwroot/current/dotserver/tomcat-$TOMCAT_VERSION/logs/catalina.out

chown -R dotcms:dotcms /opt/dotcms

systemctl start dotcms