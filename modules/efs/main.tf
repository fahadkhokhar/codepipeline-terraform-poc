resource "aws_efs_file_system" "aws-efs" {
  creation_token = "${var.client-name}-${var.env}-efs"

  tags = {
    Name = "${var.client-name}-${var.env}-efs"
    Env  = var.env
  }
}

resource "aws_efs_mount_target" "aws-efs-mount" {
  count           = length(var.private-subnet-ids)
  file_system_id  = aws_efs_file_system.aws-efs.id
  subnet_id       = var.private-subnet-ids[count.index]
  security_groups = [var.efs-security-group-id]
}
