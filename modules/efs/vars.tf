variable aws-region {
  type        = "string"
  description = "The AWS region in which the resource will be created"
  default     = "eu-central-1"
}

variable "client-name" {
  type        = "string"
  description = "Name of the client to for this resource will be utilized"
  default     = "amwater-dotcms"
}

variable "env" {
  type        = "string"
  description = "Name of environment to be created for the resource"
  default     = "dev"
}

variable "vpc-id" {
  type        = "string"
  description = "Id of the VPC in which the resources will be created"
}

variable "private-subnet-ids" {
  type        = "list"
  description = "List of subnet IDs to be used"
}

variable "efs-security-group-id" {
  type        = "string"
  description = "Id of security group which is allowed access to the EFS resources"
}

variable dependencies {
  default     = []
  description = "Any dependencies for resource creation"
  type        = "list"
}
