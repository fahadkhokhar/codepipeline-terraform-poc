output "default-vpc-id" {
  value = "${var.vpc-id}"
}

output "load-balancer-dns-name" {
  value = "${module.ec2.load-balancer-dns-name}"
}
