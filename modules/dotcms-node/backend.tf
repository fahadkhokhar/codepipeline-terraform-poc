terraform {
  backend "s3" {
    bucket = "dotcms-amwater-backend"
    region = "us-east-1"
    key    = "dotcms-amwater.tfstate"
  }
}
