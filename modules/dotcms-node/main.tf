provider "aws" {
  region = "${var.aws-region}"
}

resource "aws_key_pair" "ec2-key" {
  key_name   = "${var.client-name}-${var.env}-key"
  public_key = "${file("${var.public-key}")}"
}

module "security-groups" {
  source                      = "../security-groups"
  aws-region                  = "${var.aws-region}"
  client-name                 = "${var.client-name}"
  env                         = "${var.env}"
  vpc-id                      = "${var.vpc-id}"
  load-balancer-inbound-cidr  = var.load-balancer-inbound-cidr
  rds-port                    = "${var.rds-port}"
  ssh-security-group          = "${var.ssh-security-group}"
}

module "efs" {
  source                = "../efs"
  vpc-id                = "${var.vpc-id}"
  aws-region            = "${var.aws-region}"
  client-name           = "${var.client-name}"
  env                   = "${var.env}"
  efs-security-group-id = "${module.security-groups.efs-security-group-id}"
  private-subnet-ids    = var.private-subnet-ids
}

module "rds" {
  source                = "../rds"
  aws-region            = "${var.aws-region}"
  client-name           = "${var.client-name}"
  env                   = "${var.env}"
  vpc-id                = "${var.vpc-id}"
  rds-user              = "${var.rds-user}"
  rds-password          = "${var.rds-password}"
  rds-schema            = "${var.rds-schema}"
  rds-port              = "${var.rds-port}"
  rds-security-group-id = "${module.security-groups.rds-security-group-id}"
  rds-instance-class    = "${var.rds-instance-class}"
  allocated_storage     = "${var.rds-allocated-storage}"
  rds-snapshot-id       = "${var.rds-snapshot-id}"
  subnet-group-id       = "${var.rds-subnet-group-id}"
}

module "ec2" {
  source                          = "../ec2"
  aws-region                      = "${var.aws-region}"
  client-name                     = "${var.client-name}"
  env                             = "${var.env}"
  vpc-id                          = "${var.vpc-id}"
  private-subnet-ids              = var.private-subnet-ids
  public-subnet-ids               = var.public-subnet-ids
  ec2-launch-arn                  = var.ec2-launch-arn
  autoscaling-linked-role-arn     = "${var.autoscaling-linked-role-arn}"
  efs-id                          = "${module.efs.efs-id}"
  rds-url                         = "${module.rds.rds-instance-url}"
  rds-user                        = "${var.rds-user}"
  rds-password                    = "${var.rds-password}"
  rds-schema                      = "${var.rds-schema}"
  license-file-name               = "${var.license-file-name}"
  license-software-bucket         = "${var.license-software-s3-bucket}"
  ec2-instance-security-group-id  = "${module.security-groups.ec2-instance-security-group-id}"
  load-balancer-security-group-id = "${module.security-groups.load-balancer-security-group-id}"
  ec2-cluster-security-group-id   = "${module.security-groups.ec2-cluser-security-group-id}"
  key-name                        = "${aws_key_pair.ec2-key.key_name}"
  dependencies                    = ["${module.rds.rds-instance-id}", "${module.efs.efs-id}"]
  ec2-instance-type               = var.ec2-instance-type
  max_size                        = var.scaling-max
  min_size                        = var.scaling-min
  dotcms-version                  = "${var.dotcms-version}"
}
