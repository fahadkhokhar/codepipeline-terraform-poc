variable aws-region {
  type        = "string"
  description = "The AWS region in which the resource will be created"
  default     = "eu-central-1"
}

variable "client-name" {
  type        = "string"
  description = "Name of the client to for this resource will be utilized"
  default     = "amwater-dotcms"
}

variable "ec2-launch-arn" {
  type = "string"
  description = "The arn for the ec2 launch template"
  default = "arn:aws:iam::541676414377:instance-profile/ec2-dotcms"
}

variable "autoscaling-linked-role-arn" {
  type = "string"
  description = "The arn for the autoscaling group"
  default = "arn:aws:iam::541676414377:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling"
}

variable "env" {
  type        = "string"
  description = "Name of environment to be created for the resource"
  default     = "dev"
}

variable "internal-facing" {
  type        = "string"
  description = "Dictates whether the load balancer will be accessible via any IP"
  default     = "false"
}

variable "load-balancer-inbound-cidr" {
  type        = "list"
  description = "The CIDR blocks to allow inbound traffic to the load balancer - defaults to any"
  default     = [
    "0.0.0.0/0"
  ]
}

variable "license-software-s3-bucket" {
  type        = "string"
  description = "The s3 bucket that holds the license file and dotCMS zip files"
  default     = "com-amwater-dotcms-software"
}

variable "license-file-name" {
  type        = "string"
  description = "The name of the license file to use in the s3 bucket in ec2-create-data.tpl - i.e. s3://com-amwater-dotcms-software/LICENSE_FILE_PATH"
}

variable "rds-instance-class" {
  type        = "string"
  description = "The instance class for the database"
  default     = "db.t3.large"
}

variable "rds-snapshot-id" {
  type        = "string"
  description = "The instance class for the database"
  default     = ""
}

variable "rds-schema" {
  type        = "string"
  description = "The name of the schema in the RDS"
  default     = "dotcms"
}

variable "rds-user" {
  type        = "string"
  description = "The user name to use when connecting to the RDS"
}

variable "rds-password" {
  type        = "string"
  description = "The password to use when connecting to the RDS"
}

variable "rds-port" {
  type        = "string"
  description = "The port that the RDS listens on"
  default     = "5432"
}

variable "rds-allocated-storage" {
  type = "string"
  description = "The amount of storage allocated for the database in GB"
  default = "20"
}

variable "rds-subnet-group-id" {
  type = "string"
  description = "The subnet group id for the rds instance"
  default = "default"
}

variable "ssh-security-group" {
  type        = "string"
  description = "The security group id to allow ssh from"
  default     = ""
}

variable "vpc-id" {
  type        = "string"
  description = "The ID of the VPC"
}

variable "private-subnet-ids" {
  type        = "list"
  description = "The list of private subnet IDs that the EC2 instances and EFS mounts will be on."
}

variable "public-subnet-ids" {
  type        = "list"
  description = "The list of public subnet IDs that the public facing loadbalancer will be on."
}

variable "public-key" {
  type        = "string"
  description = "The location of the public key"
}

variable "ec2-instance-type" {
  type = "string"
  description = "The instance type for the EC2"
  default = "t3.large"
}

variable "scaling-max" {
  type = "string"
  description = "The maximum number of instances"
  default = "1"
}

variable "scaling-min" {
  type = "string"
  description = "The minimum number of instances"
  default = "1"
}

variable dependencies {
  default     = []
  description = "Any dependencies for resource creation"
  type        = "list"
}

variable "dotcms-version" {
  type        = "string"
  description = "The version of dotCMS to pull from the S3 bucket, com-amwater-dotcms-software. The expected name is dotcms_DOTCMS-VERSION.tar.gz"
  default     = "5.1.6"
}

